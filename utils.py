import subprocess
import json
import threading
import requests
import sys
import traceback
import logging
import os
from datetime import datetime
from snet import sdk
import time
from config import config, rpc_endpoints
import ast

class utils():
    def __init__(self,service,rpc_endpoints,proxy_port):
        self.group_name = "default_group"
        self.count=0
        self.call_count=0
        self.rpc_endpoints=rpc_endpoints
        self.snet_sdk = sdk.SnetSDK(config)
        self.start_time=time.time()
        self.proxy_port=proxy_port
        self.change_keys_count=0

    def create_channel(self,name):
            subprocess.check_output(["bash","script.sh","create_channel",str(name)])

    def account_setup(self,name):
        #switch account to user identity
        try:
            value=subprocess.check_output( ["bash","script.sh","account_switch",str(name)])
        except Exception as e:
            logging.exception("message",e)
            return "account doesn't exist"
        #switch network
        value=subprocess.check_output( ["bash","script.sh","network_switch"])
        #get channel
        value=subprocess.check_output( ["bash","script.sh","get_channel"])
        channel_id=str(value).split(" ")[8]
        return channel_id
    
    def account_deposit(self):
        self.count=1
        try:
            value=subprocess.check_output( ["bash", "script.sh","deposit"])
        except Exception as e:
            logging.exception("message",e)
        finally:
            self.count=0
    
    def call(self,list_command,channel_id):
        try:
            value=subprocess.check_output(list_command,stderr=subprocess.PIPE)
        except subprocess.CalledProcessError as e:
            value = sys.exc_info()[0]
            current_block=int(subprocess.check_output(["snet","channel","block-number"],
                                stderr=subprocess.PIPE))
            channel_block=self.channel_block()
            unspent_amount=self.unspent_amount(channel_id)
            if current_block>channel_block or unspent_amount==0:
                subprocess.check_output(["bash","script.sh","extend_date",str(channel_id)])
                value=subprocess.check_output(list_command)
            else:
                value=e.output.decode(sys.getfilesystemencoding())
                logging.exception("message:"+str(value))
        return value
    
    def change_keys(self,req,proc,stub,org_id,service_id):
        if self.change_keys_count==0:
            self.change_keys_count=1
            for rpc_endpoint in self.rpc_endpoints:
                if self.rpc_endpoints.index(rpc_endpoint)>=self.rpc_endpoints.index(config['eth_rpc_endpoint']):
                    config['eth_rpc_endpoint']=rpc_endpoint
                    try:
                        logging.info("current eth_rpc_endpoint "+config['eth_rpc_endpoint'])
                        self.snet_sdk = sdk.SnetSDK(config)
                        if rpc_endpoint==self.rpc_endpoints[-1]:
                            config['eth_rpc_endpoint']=self.rpc_endpoints[0]
                        service_client = self.snet_sdk.create_service_client(org_id, service_id,stub,
                                                            group_name=self.group_name,
                                                            concurrent_calls=10)
                        response = getattr(service_client.service, proc)(req)
                        break
                    except Exception as e:
                        logging.error(str(e))
            self.change_keys_count=0
        else:
            logging.error("message: another transaction is changing rpc_endpoint")
        return response

    def call_sdk(self,req,proc,stub,org_id,service_id):
        if((time.time()-self.start_time)>=3600):
            logging.info("current eth_rpc_endpoint"+config['eth_rpc_endpoint'])
            try:
                response=self.change_keys(req,proc,stub,org_id,service_id)
            except Exception as e:
                logging.exception(str(e))
            self.start_time=time.time()
            return str(response)   

        self.call_count+=1

        service_client = self.snet_sdk.create_service_client(org_id, service_id,stub,
                                                            group_name=self.group_name,
                                                            concurrent_calls=10)
        try:
            response = getattr(service_client.service, proc)(req)
        except TypeError:
            if self.count==0:
                self.account_deposit()
                response = getattr(service_client.service, proc)(req)
            else:
                time.sleep(300)
                response = getattr(service_client.service, proc)(req)
        except requests.exceptions.HTTPError:
            response=self.change_keys(req,proc,stub,org_id,service_id)
        except:
            response = getattr(service_client.service, proc)(req)

        self.call_count-=1
        return response

    def handle_channel(self,name):
        try:
            return self.account_setup(name)
        except:
            try:
                self.create_channel(name)
                return self.account_setup(name)
            except Exception as e:
                logging.exception("message:"+str(e))
                return "no_balance"

    def channel_block(self):
        channel_block=subprocess.check_output(["snet", "channel", "print-all-filter-sender"])
        channel_block=int(str(channel_block).split("\\n")[-2].split(" ")[-1])
        return channel_block

    def unspent_amount(self,channel_id):
        f=open("constants.json")
        daemon_endpoint=json.load(f)['daemon_endpoint']
        unspent_amount=subprocess.check_output(["snet","client","get-channel-state",
                                                str(channel_id),str(daemon_endpoint)])
        unspent_amount=int(str(unspent_amount).split("\\n'")[-2].split(" ")[-1])
        return unspent_amount


